package org.example.lab3;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;

import static io.restassured.RestAssured.given;

public class LabThree {
    private static final String baseUrl = "https://5deb647f-e351-4861-93a9-7196f7e0b708.mock.pstmn.io";

    @BeforeClass
    public static void setup() {
        RestAssured.baseURI = baseUrl;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.requestSpecification = new RequestSpecBuilder().setContentType(ContentType.JSON).build();
        RestAssured.responseSpecification = new ResponseSpecBuilder().build();
    }

    @Test
    public void verifyGetSuccess(){
        Response response = given().get(baseUrl + "/ownerName/success");

        response.then().statusCode(HttpStatus.SC_OK);
        RestAssured.requestSpecification.sessionId(response.jsonPath().get("name").toString().replaceAll("[^0-9]", ""));
    }

    @Test
    public void verifyPutError(){
        Response response = given().put(baseUrl + "/updateMe");

        response.then().statusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR);
    }

    @Test
    public void verifyPostPermissions(){
        Response response = given().post(baseUrl + "/createSomething?permission=yes");

        response.then().statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void verifyPostNoPermissions(){
        Response response = given().post(baseUrl + "/createSomething");

        response.then().statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    public void verifyDelete(){
        Response response = given().delete(baseUrl + "/deleteWorld");

        response.then().statusCode(HttpStatus.SC_GONE);
    }
}
