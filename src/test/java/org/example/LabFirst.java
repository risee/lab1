package org.example;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

public class LabFirst
{
    private static WebDriver chromeDriver;

    private static final String baseUrl = "https://rozetka.com.ua/ua/";

    @BeforeClass
    public static void setUp(){
        WebDriverManager.chromedriver().setup();

        ChromeOptions chromeOptions = new ChromeOptions();

        chromeOptions.addArguments("--start-fullscreen");
        chromeOptions.setImplicitWaitTimeout(Duration.ofSeconds(15));
        chromeDriver = new ChromeDriver(chromeOptions);
    }

    @BeforeMethod
    public void preconditions() {
        chromeDriver.get(baseUrl);
    }

    @AfterClass
    public static void tearDown() { chromeDriver.quit();}

    @Test
    public void testClick(){
        WebElement categoryMenu = chromeDriver.findElement(By.id("fat-menu"));
        categoryMenu.click();
    }

    @Test
    public void testSearch() {
        WebElement searchLine = chromeDriver.findElement(By.className("search-form__input"));
        searchLine.sendKeys("Testing");
        Assert.assertEquals(searchLine.getAttribute("value"), "Testing");
    }

    @Test
    public void testNotXPATH(){
        WebElement header = chromeDriver.findElement(By.id("fat-menu"));

        Assert.assertNotNull(header);
    }

    @Test
    public void testTile(){
        WebElement tiles = chromeDriver.findElement(By.className("tile"));

        Assert.assertTrue(tiles.isDisplayed());
    }
}
