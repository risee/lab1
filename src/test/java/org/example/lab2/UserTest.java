package org.example.lab2;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.devtools.v121.fetch.model.AuthChallengeResponse;

import java.util.Map;

import static io.restassured.RestAssured.given;

public class UserTest {
    private static final String baseUrl = "https://petstore.swagger.io/v2";

    private static final String PET = "/pet";
    private static final String PET_ID = PET + "/{petId}";

    private Integer petId;

    @BeforeClass
    public static void setup() {
        RestAssured.baseURI = baseUrl;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.requestSpecification = new RequestSpecBuilder().setContentType(ContentType.JSON).build();
        RestAssured.responseSpecification = new ResponseSpecBuilder().build();
    }

    @Test
    public void verifyPostPet(){
        Map<String, ?> body = Map.of(
                "id", 122,
                "name", "YevhenC",
                "status", "available"
        );

        Response response = given().body(body).post(PET);

        response.then().statusCode(HttpStatus.SC_OK);
        RestAssured.requestSpecification.sessionId(response.jsonPath().get("id").toString().replaceAll("[^0-9]", ""));
    }

    @Test
    public void verifyGetPet(){
        Map<String, ?> body = Map.of(
        );
        Response response = given().pathParam("petId", 122).body(body).get(PET_ID);

        response.then().statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void verifyPutPet(){
        Map<String, ?> body = Map.of(
                "id", 122,
                "name", "CYevhen",
                "status", "available"
        );

        Response response = given().body(body).put(PET);

        response.then().statusCode(HttpStatus.SC_OK);
        RestAssured.requestSpecification.sessionId(response.jsonPath().get("id").toString().replaceAll("[^0-9]", ""));
    }
}
